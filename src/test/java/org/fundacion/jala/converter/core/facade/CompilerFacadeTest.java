package org.fundacion.jala.converter.core.facade;

import org.fundacion.jala.converter.core.exceptions.CompilerException;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class CompilerFacadeTest {

    @Test @Ignore
    public void facadeJavaCompile() throws CompilerException {
        assertTrue("".equals(CompilerFacade.facadeJavaCompile("")));
    }

    @Test @Ignore
    public void facadePythonCompile() throws CompilerException {
        assertEquals("", CompilerFacade.facadePythonCompile(""));
    }

    @Test @Ignore
    public void facadeNodejsCompile() throws CompilerException {
        assertEquals("", CompilerFacade.facadeNodejsCompile(""));
    }
}