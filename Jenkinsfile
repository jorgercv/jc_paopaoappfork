pipeline {
  agent any
  stages {
    stage('Build project') {
      steps {
        sh 'chmod +x gradlew'
        sh './gradlew clean build'
      }
    }

    stage('Package') {
      steps {
        sh 'docker build -t paopaoapp:2.2 .'
      }
    }
    stage('Quality control') {
      steps {
        sh 'chmod +x gradlew'
        withSonarQubeEnv('SonarQube') {
                    sh 'chmod +x gradlew'
                    sh "./gradlew sonarqube \
                       -Dsonar.projectKey=PaoPao \
                       -Dsonar.host.url=http://192.168.33.20:9000 \
                       -Dsonar.login=${TOKEN}"
        }
      }
    }
    stage('Quality gate') {
      steps {
        waitForQualityGate abortPipeline: true
      }
    }
    stage('Publish') {
      steps {
        sh 'docker login -u "${DOCKER_HUB_PASS_USR}" -p "${DOCKER_HUB_PASS_PSW}"'
        sh 'docker image tag paopaoapp:2.2 jorgercv/paopaojenkinsmade:2.2'
        sh 'docker image push jorgercv/paopaojenkinsmade:2.2'
      }
    }
    stage('Deploy') {
      steps {
        sh 'docker-compose up -d --force-recreate'
      }
    }

  }
  environment {
    DOCKER_HUB_PASS = credentials('dockehubpass')
    TOKEN = credentials('sonar_token')
    EMAIL_TO = 'jorge.caceres@fundacion-jala.org'
  }
  post {
    always {
      archiveArtifacts(artifacts: 'build/reports/tests/test/index.html', fingerprint: true)
      archiveArtifacts(artifacts: 'build/libs/*.jar', fingerprint: true)
      junit 'build/test-results/test/*.xml'
    }
    success {
      emailext body: 'Check console output at $BUILD_URL to view the results. \n\n ${CHANGES} \n\n -------------------------------------------------- \n${BUILD_LOG, maxLines=100, escapeHtml=false}',
                to: "${EMAIL_TO}",
                subject: 'Jenkins build is ok: $PROJECT_NAME - #$BUILD_NUMBER - $BUILD_STATUS'
    }
    failure {
      emailext body: 'Check console output at $BUILD_URL to view the results. \n\n ${CHANGES} \n\n -------------------------------------------------- \n${BUILD_LOG, maxLines=100, escapeHtml=false}',
               to: "${EMAIL_TO}",
               subject: 'Build failed in Jenkins: $PROJECT_NAME - #$BUILD_NUMBER - $BUILD_STATUS'
    }
    unstable {
      emailext body: 'Check console output at $BUILD_URL to view the results. \n\n ${CHANGES} \n\n -------------------------------------------------- \n${BUILD_LOG, maxLines=100, escapeHtml=false}',
               to: "${EMAIL_TO}",
               subject: 'Unstable build in Jenkins: $PROJECT_NAME - #$BUILD_NUMBER - $BUILD_STATUS'
    }
    changed {
      emailext body: 'Check console output at $BUILD_URL to view the results. \n\n ${CHANGES} \n\n -------------------------------------------------- \n${BUILD_LOG, maxLines=100, escapeHtml=false}',
               to: "${EMAIL_TO}",
               subject: 'Jenkins build status has changed: $PROJECT_NAME - #$BUILD_NUMBER - $BUILD_STATUS'
    }
  }
}
